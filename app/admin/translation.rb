ActiveAdmin.register I18n::Backend::ActiveRecord::Translation do
  permit_params :locale, :key, :value

  form do |f|
    f.inputs do
      f.input :locale
      f.input :key
      f.input :value
      f.actions
    end
  end

  after_update do
    I18n.reload!
  end
end
