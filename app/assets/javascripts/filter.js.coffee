# Cleans up filter submission, for cleaner URL
$(document).on 'turbolinks:load', ->
	$('body.pages form :input').prop 'disabled', false

	$('body.pages form').submit ->
		$('input[name=utf8]').prop 'disabled', true
		$(':input', this).filter ->
			this.value.length == 0 && this.name != 'region'
		.prop 'disabled', true
