require File.expand_path('boot', __dir__)

require 'rails/all'
require 'action_view/encoded_mail_to/mail_to_with_encoding'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AgendaDuLibreRails
  # All the specific configuraton for ADL
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    # Settings in config/environments/* take precedence over those specified
    # here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record
    # auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names.
    # Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from
    # config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path +=
    #   Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.load_path +=
      Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = :fr
    config.i18n.available_locales = %i[fr en 'pt-BR']

    config.action_mailer.default_options = {
      from: 'moderateurs@agendadulibre.org',
      to: 'moderateurs@agendadulibre.org'
    }

    config.action_dispatch.default_headers['X-Frame-Options'] = 'ALLOWALL'

    # In rails 4, plugin and vendor images need to be precompiled
    config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif]

    # Rails 5 now defaults to csrf tokens per form, which breaks adl for the
    # time being
    # TODO
    config.action_controller.per_form_csrf_tokens = false
  end
end
