require 'test_helper'

# Test some event helper methods
class EventsHelperTest < ActionView::TestCase
  test 'Markdown with empty content' do
    assert_equal '', to_markdown('')
    assert_equal '', to_markdown('  ')
    assert_equal "'", to_markdown("\'")
    assert_equal '', to_markdown('
')
  end
end
