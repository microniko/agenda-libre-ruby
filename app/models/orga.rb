# Organisations related to this agenda
class Orga < ApplicationRecord
  acts_as_taggable
  strip_attributes
  has_paper_trail ignore: %i[last_updated secret submitter decision_time
                             lock_version latitude longitude]

  belongs_to :region
  belongs_to :kind

  validates :kind, presence: true
  validates :name, presence: true
  validates :region, presence: true
  validates :url, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :diaspora, allow_blank: true, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :feed, allow_blank: true, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :contact, allow_blank: true, email: true
  validates :submitter, allow_blank: true, email: true

  geocoded_by :full_address
  # after_validation :geocode, if: -> (obj) { obj.saved_change_to_address? }
  after_validation :geocode

  scope :active, -> { where active: true }
  scope :moderated, -> { where moderated: true }
  scope :unmoderated, -> { where moderated: false }

  # Only present to simplify maps_controller, to have the same scopes as events
  scope :locality, -> {}
  scope :daylimit, -> {}
  scope :future, -> {}
  scope :period, ->(_year, _week) {}

  scope :kind, ->(kind) { where kind: kind }
  scope :region, (lambda do |region|
    return if region.nil? || region == 'all' || region.to_i.zero?
    temp = Region.find region
    where region: [temp, temp.regions].flatten
  end)
  scope :tag, ->(tag) { tagged_with tag }
  scope :geo, -> { where 'latitude is not null and longitude is not null' }
  scope :active, -> { where active: true }

  before_validation do
    # Populate submitter using contact info if absent
    self.submitter ||= contact
    self.secret ||= SecureRandom.urlsafe_base64(32)[0...32]
    self.submission_time ||= Time.zone.now
  end

  after_create do
    send_secret if submitter.present?
    # Send email to moderators when an new orga is received
    ModerationorgaMailer.create(self).deliver_now!
  end

  after_update do
    send_secret if saved_change_to_secret? || saved_change_to_submitter?

    if saved_change_to_moderated?
      OrgaMailer.accept(self).deliver_now!
      # Send email to moderators when an orga is accepted
      ModerationorgaMailer.accept(self).deliver_now!
    else
      OrgaMailer.update(self).deliver_now!
      # Send email to moderators when an orga is updated
      ModerationorgaMailer.update(self).deliver_now!
    end
  end

  before_destroy do
    OrgaMailer.destroy(self).deliver_now! if submitter.present?
    # Send email to moderators when an orga is deleted
    ModerationorgaMailer.destroy(self).deliver_now! if submitter.present?
  end

  def send_secret
    OrgaMailer.create(self).deliver_now!
  end

  def name_as_tag
    name.gsub(/\AL'/, '').gsub(/[\s\*'\.]/, '-').delete ':'
  end

  def as_json(_options = {})
    { type: 'Feature', properties: {
      id: id, icon: kind.icon, name: name,
      place_name: place_name, address: address, city: city,
      tags: tags,
      popupContent: "<a href=\"/orgas/#{id}\">#{self}</a>"
    }, geometry: { type: 'Point', coordinates: [longitude, latitude] } }
  end

  def full_address
    [address, city, region, region.try(:region)].compact.join ', '
  end

  def to_s
    "[#{kind.name}] #{name}"
  end
end
