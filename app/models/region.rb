# This is mostly to group events around a region
class Region < ApplicationRecord
  belongs_to :region
  has_many :regions, dependent: :nullify

  has_many :orgas, dependent: :destroy

  default_scope { order :name }
  scope :local, ->(*) { where 'url IS NULL OR url = \'\'' }
  scope :region, (lambda do |region|
    return if region.nil? || region == 'all' || region.to_i.zero?
    temp = Region.find region
    where region: [temp, temp.regions].flatten
  end)

  def to_s
    name
  end
end
