# Add more data to events, with an optionnal place name
class AddPlaceNameToEvents < ActiveRecord::Migration
  def change
    add_column :events, :place_name, :string
  end
end
