# Preview all emails at http://localhost:3000/rails/mailers/event_mailer
class EventMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/event_mailer/create
  def create
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    EventMailer.create Event.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/event_mailer/accept
  def accept
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    EventMailer.accept Event.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/event_mailer/update
  def update
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    event = Event.first

    event.start_time += 1.day
    event.description = event.description + '<p>     et</p>
<p> hello world</p>'

    EventMailer.update event
  end

  # Preview this email at http://localhost:3000/rails/mailers/event_mailer/destroy
  def destroy
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    event = Event.last
    event.reason = 'hello world'
    EventMailer.destroy event
  end
end
