# Adding parent regions
class AddParentRegionToRegion < ActiveRecord::Migration
  def change
    add_reference :regions, :region, index: true, foreign_key: true
    add_column :regions, :code, :string
    add_column :regions, :url, :string

    reversible do |dir|
      dir.up { create_countries }
      dir.down { destroy_countries }
    end
    to = current_instance
    Region.where(url: nil).each { |r| r.update region_id: to } if to
  end

  private

  # Creates the 5 countries corresponding to the 5 main adl instances
  def create_countries
    say 'Creates one country/region per (known) agenda: BE, BR, FR, CA-QC, CH'
    [
      { name: 'Belgique', code: :BE, url: '//www.agendadulibre.be' },
      { name: 'Brasil', code: :BR, url: 'http://agenda.softwarelivre.org' },
      { name: 'France', code: :FR, url: '//www.agendadulibre.org' },
      { name: 'Québec', code: 'CA-QC', url: 'http://agendadulibre.qc.ca' },
      { name: 'Suisse', code: :CH, url: '//www.agendadulibre.ch' }
    ].each { |country| Region.create country }
  end

  # Attemps to find the proper country associated with your instance
  def current_instance
    [
      ['Namur', :BE],
      ['Acre', :BR],
      ['Montréal', 'CA-QC'],
      ['Berne', :CH],
      ['Bretagne', :FR]
    ].find do |data|
      return Region.find_by(code: data[1]).id if Region.exists? name: data[0]
    end
  end

  # Removes the 5 countries corresponding to the main known instances
  def destroy_countries
    Region.find_by(name: 'Belgique').try :destroy
    Region.find_by(name: 'Brasil').try :destroy
    Region.find_by(name: 'France').try :destroy
    Region.find_by(name: 'Québec').try :destroy
    Region.find_by(name: 'Suisse').try :destroy
  end
end
