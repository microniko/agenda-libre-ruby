# Helper for the digest/markdown views
module DigestHelper
  # Important mechanism, to ensure the digest will not have the same footnote
  # link multiple times
  def renumber_footnotes(id, description)
    return if description.nil? || description.blank?
    description.gsub(/\[(\d)+\]/, "[#{id}_\\1]")
  end
end
