# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the
# db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

City.create name: 'Rennes'
Region.create name: 'Bretagne'
Region.create name: 'My very first region'
User.create login: 'admin@example.com', email: 'admin@example.com',
            password: 'password'

AdminUser.create email: 'admin@example.com', password: 'password'

Kind.create name: 'association', icon: 'sitemap'
Kind.create name: 'enterprise', icon: 'building'
Kind.create name: 'glug', icon: 'support'
Kind.create name: 'provider', icon: 'tty'
Kind.create name: 'institution', icon: 'institution'

# rubocop:disable Metrics/LineLength
I18n::Backend::ActiveRecord::Translation.create(
  [
    { locale: 'fr', key: 'mail_suffix', value: '[AdL] ' },

    { locale: 'fr', key: 'layouts.application.subtitle',
      value: 'Les événements du Libre en France' },

    { locale: 'fr', key: 'events.new.subtitle',
      value: "Cette page permet de soumettre un événement dans l'Agenda du Libre. Celui-ci n'apparaîtra pas automatiquement dans l'Agenda, il sera tout d'abord validé par un modérateur. Un courrier électronique vous sera envoyé à l'adresse e-mail de contact donnée ci-dessous lorsque l'événement aura été modéré." },

    { locale: 'fr', key: 'events.new.advises',
      value: "# Recommandations importantes

Ces quelques recommandations vous permettront de proposer un événement qui sera validé plus rapidement, et dont la lisibilité sera meilleure pour les utilisateurs de l'Agenda du Libre.

* L'événement doit concerner le **Libre**. L'agenda n'a pas vocation à publier d'autres types d'événements.
* Le texte des événements doit être rédigé en français, en **évitant les fautes de grammaire et d'orthographe**.
* Donnez une description **rédigée** de l'événement, avec des phrases, plutôt qu'un style télégraphique.
* Commencez la description de votre événement par une phrase reprenant les informations principales, telle que: _«L'association [Zorglub](http://www.asso-zorglub.org) organise une conférence sur le thème des **modèles économiques du Logiciel Libre** le samedi 21 mars de 18h à 20h à la médiathèque Champillion de Montpellier.»_
* Pensez que le lecteur peut ne pas connaître votre association, le logiciel dont il sera question, etc., donc donnez à chaque fois toutes les informations nécessaires. Rappelez **le principe de l'événement, le public visé, le lieu exact, la date, l'heure**, même si il est régulier (repas, rencontre régulière, etc.) et que vous avez déjà soumis un événement du même type dans l'Agenda.
* **Égayez au maximum la description** de votre événement: lien vers le site de votre association, vers le logiciel ou le langage dont il sera question, vers les organismes partenaires, etc.
* Ne mettez pas l'intégralité des mots en majuscule, que ce soit dans le titre, le nom de la ville ou la description.
* Donnez l'**adresse directe** d'un site Web donnant plus d'informations sur l'événement. Si l'adresse est la page d'accueil du site de votre organisation, veillez à ce que l'événement soit clairement visible, et que le lecteur n'ait pas à chercher trop longtemps avant de trouver l'information qu'il cherche.
* La description de l'événement ne doit pas ressembler à une publicité éhontée pour votre entreprise, projet ou logiciel. Le ton d'un communiqué de presse, par exemple, ne convient pas pour l'Agenda du Libre.
* Les événements de type professionnel sont acceptés dès lors qu'ils sont accessibles et ouverts à tous. Les événements payants sont acceptés si le prix de l'entrée ne réserve pas l'événement à un public uniquement professionnel.

L'équipe de modération se réserve le droit de modifier la description de l'événement pour la rendre plus complète, plus lisible ou plus attrayante.

Si vous soumettez souvent un événement régulier dans l'Agenda du Libre, vous pouvez automatiser cette procédure à l'aide d'un [script que nous vous proposons](/adl-submit.py).
" },

    { locale: 'fr', key: 'events.form.title_helper',
      value: 'Décrivez en moins de 5 mots votre événement, sans y indiquer le lieu, la ville ou la date' },

    { locale: 'fr', key: 'events.form.description_helper',
      value: '**Décrivez de la manière la plus complète possible votre événement.**' },

    { locale: 'fr', key: 'events.form.url_helper',
      value: "Lien **direct** vers une page donnant plus d'informations sur l'événement" },

    { locale: 'fr', key: 'events.form.contact_helper',
      value: '*Adresse e-mail de contact. Elle sera affichée de manière peu compréhensible par les spammeurs.*' },

    { locale: 'fr', key: 'events.form.submitter_helper',
      value: "*Adresse e-mail du soumetteur de l'événement. Elle ne sera utilisée que par les modérateurs pour contacter la personne ayant proposé l'événement, pour l'informer de sa validation ou de son rejet. Si cette adresse n'est pas présente, l'adresse de contact sera utilisée*" },

    { locale: 'fr', key: 'events.form.tags_helper',
      value: "*Tags pour l'événement. Les tags sont séparés par des espaces. Un tag ne peut contenir que des lettres minuscules, des chiffres et des tirets.*

*Dans les tags, indiquez le nom de la ou des associations organisatrices. N'indiquez pas le nom de la ville ou de la région.*" },

    { locale: 'fr', key: 'pages.infos.content',
      value: "## <em class='fa fa-info'></em> Informations

### Table des matières

* [Pourquoi?](#pourquoi)
* [Comment?](#comment)
* [FAQ](#faq)
* [Contributeurs et auteurs](#contributeurs-et-auteurs)
* [Modérateurs](#moderateurs)
* [Autres utilisateurs](#autres-utilisateurs)
* [Historique](#historique)

### <a name='pourquoi'>Pourquoi?</a>

Auparavant, le site Agenda.Lolix.org, géré par Rodolphe Quiedeville, permettait de disposer d'un agenda des événements locaux organisés autour du Logiciel Libre. Depuis sa décision d'arrêter de maintenir ce site, cet agenda manquait, et le nombre de dépêches postées sur [LinuxFr](http://linuxfr.org) à propos d'événements locaux est devenu trop important.

Il a donc semblé pertinent de relancer un site avec la même idée: un simple agenda des manifestations autour du Logiciel Libre.

### <a name='comment'>Commment?</a>

Le site de l'[Agenda du Libre](http://www.agendadulibre.org) fonctionne avec un bout de code spécifique écrit en Ruby on Rails, et distribué selon les termes de la licence AGPL.

Il a été développé très rapidement, et est largement améliorable, aussi bien du point de vue de la qualité du code que des fonctionnalités. Il reste sans aucun doute de très nombreux bugs. N'hésitez pas à contribuer au développement de ce petit bout de code. Un [dépôt Git](http://gitorious.org/agenda-du-libre-rails) est disponible sur Gitorious pour contribuer au code du logiciel.

Les discussions autour des améliorations à apporter au site ont lieu sur la liste de diffusion devel@agendadulibre.org ([inscription](https://listes.agendadulibre.org/wws/subscribe/devel)).

### <a name='faq'>F.A.Q.: Questions fréquemment posées</a>

#### Pourquoi y'a-t-il une étape de modération? La validation pourrait être automatique, non?

Évidemment, techniquement, la validation d'un événement pourrait être instantanée, dès que l'événement est soumis. En fait, en pratique, cela n'est pour l'instant pas vraiment envisageable si l'on souhaite conserver une certaine qualité à l'Agenda du Libre. De trop nombreuses soumissions d'événements sont incomplètes (donc incompréhensibles pour le commun des mortels), dans un style télégraphique, ou alors contiennent un certain nombre de fautes d'orthographe ou de problèmes de mise en page. Si l'on souhaite conserver une certaine cohérence dans l'Agenda du Libre, alors une modération est vraiment nécessaire.

D'autre part, une modération a posteriori n'est pas possible à cause des flux RSS. En effet, dès qu'un événement est validé, il apparaît dans les flux RSS, et est donc chargé par tous les aggrégateurs des personnes abonnées au flux en question. Il est alors trop tard pour corriger des erreurs, ou supprimer cet événement si nécessaire.

Aujourd'hui, la modération est réalisée par une équipe de 4 personnes, qui pourra être étendue si nécessaire. Si l'événement est dès le départ correct, la modération est très souvent réalisée en quelques heures.

#### Pourquoi n'y a-t-il que les événements Français? Pourquoi y'a-t-il seulement une case Autre pays?

L'Agenda du Libre a vocation à faire connaître auprès du plus large public les événements organisés autour du Logiciel Libre. Ces événements n'ont d'intérêt que s'ils sont accessibles d'un point de vue géographique par les visiteurs de l'Agenda du Libre.

Un Agenda du Libre de toute la francophonie n'a pas vraiment de sens: les Français ne sont pas susceptibles de se déplacer au Québec ou en Afrique francophone pour une install-party ou un repas, et vice-versa. Il nous semble donc plus pertinent que des initiatives similaires à l'Agenda du Libre se mettent en place dans d'autres pays.

Bien que l'Agenda du Libre soit dédié aux événements Français, il est vrai que nous publions parfois des événements de pays limitrophes à la France.

À noter que d'autres Agenda du Libre ont été lancés pour d'autres pays: [le Québec](http://www.agendadulibre.qc.ca), [la Belgique](http://www.agendadulibre.be) et [la Suisse](http://www.agendadulibre.ch).

#### Puis-je utiliser le logiciel de l'Agenda du Libre pour mon agenda?

Oui, bien sûr, vous le pouvez: le logiciel de l'Agenda du Libre est un Logiciel Libre, distribué sous les termes de la licence AGPL.

#### Comment faire en sorte que mon association apparaisse dans l'Agenda du Libre?

Pour apparaître sur l'Agenda du Libre, il faut à présent demander aux modérateurs de l'Agenda d'ajouter votre groupe d'utilisateurs.

Les modérateurs de l'Agenda du Libre mettent périodiquement à jour la liste des GULLs de l'Agenda du Libre. N'hésitez pas à les contacter si vous souhaitez une mise à jour plus rapide.

### <a name='contributeurs-et-auteurs'>Contributeurs et auteurs</a>

* Mélanie Bats
* [Thomas Petazzoni](http://thomas.enix.org)
* [David Mentré](http://www.linux-france.org/~dmentre), divers patches
* L'[AFUL](http://www.aful.org), pour la [liste des GULs](http://www.aful.org/gul)
* Erwan Lehérissé, pour la CSS et la favicon

### <a name='moderateurs'>Modérateurs</a>

Pour l'heure, l'équipe de modération est constituée de (par ordre alphabétique):

* Lionel Allorge, de l'[April](http://www.april.org)
* Mélanie Bats, de [Toulibre](http://www.toulibre.org)
* Christian Delage
* Olivier Fraysse, d'[Ubuntu-fr](http://www.ubuntu-fr.org)
* Magali Garnero, de l'[April](http://www.april.org) et de [Parinux](http://parinux.org)
* Erwan Lehérissé, d'[Actux](http://www.actux.org)
* Philippe Pary, de [Chtinux](http://www.chtinux.org)
* Thomas Petazzoni, de [Toulibre](http://www.toulibre.org)
* Marco Rodrigues, d'[Adeti](http://lug.adeti.org)

D'autres modérateurs pourront être ajoutés, pour assurer à l'agenda une réactivité maximale, quelle que soit la période de l'année.

Vous pouvez contacter les modérateurs en utilisant l'adresse moderateurs@agendadulibre.org.

### <a name='autres-utilisateurs'>Autres utilisateurs</a>

Bien que le logiciel faisant fonctionner l'Agenda du Libre soit spécifique, d'autres personnes ou projets ont choisi de l'utiliser comme base pour mettre en ligne un agenda:

* L'[agenda](http://agenda.jeudisepn.org) de l'association Jeudis des EPN, qui regroupe l'ensemble des événements des EPN en France. Le code source de cette version est disponible dans une branche du dépôt Subversion de l'Agenda du Libre.
* L'[agenda](http://www.assojeunes-rennes.org/agenda) du [Réseau Asso Jeune de Rennes](http://www.assojeunes-rennes.org)
* L'[agenda](http://tondeuse.eu.org/agenda) des événements de Rennes et des environs mis en place par l'association [La Tondeuse À Roazhon](http://tondeuse.eu.org).

### <a name='historique'>Historique</a>

#### 18/08/2014
* Réécriture de l'Agenda du Libre en Ruby on Rails, et migration des agendas belge, français et suisse

#### 29/10/2103
* [Hébergement de l'Agenda du Libre par l'April](http://www.april.org/lapril-heberge-lagenda-du-libre)

#### 04/07/2013
* Arrivée de deux nouveaux modérateurs: Lionel Allorge et Magali Garnero

#### 02/01/2012
* Arrivée de deux nouveaux modérateurs: Philippe Pary et Marco Rodrigues

#### 20/02/2011
* Passage à Git pour la gestion de version du code source de l'Agenda du Libre

#### 24/06/2009
* Passage du cap des [3000 événements](http://thomas.enix.org/Blog-20090624094723-Libre)

#### 26/04/2009
* Arrivée d'Olivier Fraysse dans l'équipe de modération

#### 19/02/2009
* Amélioration des messages de notification pour qu'ils s'organisent en fils de discussion.
* L'Agenda du Libre passe le [cap des 2500 événements](https://linuxfr.org/2009/02/20/25057.html)

#### 17/02/2009
* Ajout d'une action \"Ajouter à mon calendrier\" sur chaque événement. L'idée est d'avoir un lien sur chaque événement qui permet d'enregistrer juste cet événement dans son calendrier

#### 11/02/2009
* Lancement d'[agendadulibre.be](http://www.agendadulibre.be), l'Agenda du Libre pour la Belgique

#### 26/01/2009
* Amélioration de l'interface de modération pour proposer un système de petites notes permettant une meilleure communication entre les modérateurs

#### 26/09/2008
* Passage à l'encodage UTF-8 pour les pages Web, les flux RSS, les courriers électroniques envoyés et la base de données

#### 23/09/2008
* Le soumetteur d'un événement peut maintenant éditer ou annuler son événement pendant la phase de modération
* Nouvelle action de modération permettant de demander au soumetteur d'un événement d'ajouter des informations complémentaires à un événement

#### 05/09/2008
* Intégration de [Tiny MCE](http://tinymce.moxiecode.com), un éditeur HTML WYSIWYG, afin de faciliter l'accès à l'Agenda du Libre pour ceux qui ne connaissent pas le HTML. Contribution de Erwan Lehérissé

#### 09/07/2008 au 12/07/2008
* Les événements peuvent désormais être édités ou annulés par leur soumetteur après modération
* Refonte complète de la [carte](/maps), désormais basée sur un fond de carte [OpenStreetMap](http://www.openstreetmap.org), en utilisant la bibliothèque Javascript [OpenLayers](http://www.openlayers.org). Cette carte affiche les prochains événements, ainsi que la localisation des groupes d'utilisateurs
* Ajout du tag <georss:point> dans le flux RSS pour préciser la localisation géographique d'un événement. Cela peut servir à afficher les événements sur une carte, comme sur la carte de l'Agenda du Libre, ou sur Google Maps. Ce tag fait partie de la spécification GeoRSS

#### 29/06/2008
* Un courrier électronique est désormais envoyé au soumetteur dès que l'événement est enregistré et est en attente de modération

#### 08/05/2008
* Nouvelle interface de modération
* Édition des événements validés par les modérateurs
* Ajout d'Erwan Lehérissé à l'équipe de modération

#### 15/03/2008
* Mise en production d'une nouvelle CSS développée par Erwan Lehérissé

#### 25/11/2007
* Ajout d'un script permettant de récupérer la liste des événements par tag au format XML, utilisé par le site du [Libre en Fête](http://www.libre-en-fete.net)

#### 10/04/2007
* Passage du cap des 1000 événements

#### 14/01/2007
* Mise en place du système de tags sur les événements
* Statistiques par ville

#### 29/10/2006
* Mise en place du sélecteur de région pour la navigation dans l'agenda
* Affichage des flèches de navigation vers le passé ou le futur seulement si il y a des événements à voir dans le passé ou le futur

#### 09/09/2006
* Activation du https pour l'Agenda du Libre, pour ceux qui souhaitent utiliser une connexion sécurisée

#### 09/07/2006
* Ajout d'une option --test-output au script de soumission. Elle permet de récupérer une page HTML qui ressemble à ce que donnera l'événement dans l'Agenda du Libre une fois modéré. Ceux qui utilisent le script de soumission peuvent donc avoir une prévisualisation

#### 02/05/2006
* Simplification dans l'affichage des dates. Quand le jour de début et de fin sont identiques, le jour n'est indiqué qu'une seule fois

#### 04/03/2006
* Ajout d'une favicon, proposée par Air1

#### 25/02/2006
* Ajout d'une liste des questions fréquemment posées, sur cette page

#### 20/02/2006
* Publication d'une [dépêche sur LinuxFr.org](https://linuxfr.org/2006/02/20/20383.html) pour marquer le passage du cap des 300 événements recensés par l'Agenda du Libre, et pour faire connaître les nouveautés de l'Agenda depuis son lancement en juin 2005

#### 08/02/2006
* Publication d'un [article](http://www.pcinpact.com/actu/news/26517-Les-First-Jeudi-parisiens-quand-la-banquise-.htm) sur le site [PC Inpact](http://www.pcinpact.com) qui mentionne l'Agenda du Libre en ces termes: Pour savoir si votre GULL en organise un... allez voir sur sa page web et n'oubliez pas de consulter l'Agenda du Libre, une référence en matière d'événements et de rencontres sur le libre

#### 05/02/2006
* Améliorations mineures du script extract-gulls.py
* Mise en place d'une favicon basée sur le logo proposée par Air1
* Mise à jour de la documentation d'installation
* Synchronisation de la base de données des GULLs de l'Agenda du Libre avec la nouvelle version de la liste de l'AFUL

#### 01/02/2006
* Distribution de 400 tracts au salon Solutions Linux 2006, et pose de nombreuses affiches dans le village associatif du salon

#### 11/01/2006-15/01/2006
* Ajout d'une boîte de recommandations sur la page de [soumission d'un événement](/events/new)
* Correction de problèmes avec Internet Explorer au niveau de la carte et de la feuille CSS
* Affichage des GULLs de la région dans la carte, et lors de la consultation des informations sur un évènment
* Ajout d'une page de [statistiques](/stats)
* Amélioration du script de soumission et rédaction d'une [documentation](http://www.agendadulibre.org/submit-script-doc.php)

#### 21/12/2005
* Ajout d'une CSS alternative proposée par Air1

#### 17/11/2005
* Légère amélioration de la carte, un lien est maintenant disponible
* Petites améliorations sur le calendrier iCal

#### 13/11/2005
* L'Agenda du Libre est maintenant disponible sur la page d'accueil de [Framasoft](http://www.framasoft.net)

#### 05/11/2005
* Ajout d'une carte des événements par région

#### 04/10/2005
* Correction d'un problème de recouvrement mineur dans le CSS

#### 24/09/2005
* Ajout de l'Agenda du Libre dans la liste des liens disponibles dans la barre de liens du site [LinuxFr.org](http://linuxfr.org)

#### 20/09/2005
* Correction d'un bug dans la génération du calendrier iCal (oubli de stripper les backslashes ajoutés lors de l'insertion dans la base SQL)
* Mise à jour de la liste des clients iCal qui fonctionnent

#### 19/09/2005
* Correction de plusieurs bugs dans la génération des calendriers iCal (champ UID manquant, champ PRODID manquant, point-virgules à la place de deux points, etc.)
* Utilisation d'URLs en webcal:// pour les calendriers

#### 18/09/2005
* Les calendriers donnent maintenant les événements des 12 mois précédents dans le passé et jusqu'à l'infini dans le futur
* Indication d'Evolution 2.0.4 et de KOrganizer dans les clients qui fonctionnent avec les calendriers iCal
* Ajout d'une boîte rappelant aux visiteurs l'existence des flux RSS et des calendriers iCal
* Ajout de la liste des flux RSS dans l'en-tête de la page pour que les navigateurs comme Firefox les affichent automatiquement

#### 17/09/2005
* Développement de la génération de [calendriers iCal](/icallist). Comme pour les flux RSS, il y a un calendrier disponible pour chaque région, ainsi qu'un calendrier national
* Intégration des patches de validation des entrées de David Mentré
* Intégration du patch de David Mentré ajoutant le calendrier annuel
* Mise en place d'une authentification HTTP pour l'accès aux statistiques, afin d'éviter le spam de referers. Il est toujours possible d'accéder aux statistiques: le mot de passe est affiché dans la boîte de dialogue d'authentification

#### 15/09/2005
* L'Agenda du Libre est maintenant référencé par [Lea-Linux](http://www.lea-linux.org)

#### 19/08/2005
* Depuis cette date, tous les nouveaux comptes créés sur [LinuxFr](http://linuxfr.org) disposent par défaut d'une boîte Agenda du Libre affichant les événements pour les 30 jours à venir

#### 31/07/2005-18/08/2005
* Amélioration mineure de la documentation d'installation (contribution de Thierry Boudet)
* Mise en valeur du jour courant de l'Agenda (contribution de Maxime Petazzoni)
* Ajout de conseils supplémentaires pour les contributeurs d'événements
* Affichage de la portée de l'événement dans l'interface de modération, pour faciliter celle-ci

#### 30/07/2005
* Amélioration des fonctionnalités d'envoi de mail: les modérateurs reçoivent un mail à chaque soumission d'événement, à chaque édition, validation et suppression d'événement

#### 07/07/2005
* Présentation de l'Agenda du Libre aux [Rencontres Mondiales du Logiciel Libre](http://www.rencontresmondiales.org) à Dijon, dans le cadre du thème [InterLUG](http://www.interlug-fr.org). Les [slides](http://www.agendadulibre.org/svn/presentation) de la présentation sont disponibles

#### 23/06/2005
* Ajout des modérateurs

#### 15/06/2005
* Correction d'un bug dans la génération du RSS
* Amélioration de la CSS proposée par Jean-Marie Favreau

#### 13/06/2005
* Rédaction des recommendations pour la modération
* Mise en place des listes de diffusion devel@agendadulibre.org et moderateurs@agendadulibre.org
* Mise en place d'un système de prévisualisation lors de la soumission d'un événement

#### 12/06/2005
* Ajout d'un fichier bd-private.inc.php.template donnant un exemple de fichier bd-private.inc.php
* Les jours passés et les jours à venir sont maintenant de couleur différente (patch soumis par Mélanie Bats)
* Dans le mail envoyé lorsqu'un événement est modéré, un lien vers l'événement est donné (patch soumis par Mélanie Bats)
* Utilisation d'une fonction `quote_smart` pour formater correctement les arguments d'une requête SQL (patch initialement soumis par Mendolia Davide)

#### 07/06/2005
* Ajout du schéma des [tables SQL](http://www.agendadulibre.org/svn/trunk/schema.sql)

#### 05/06/2005
* Lancement du site" },

    { locale: 'fr', key: 'pages.contact.content',
      value: "## <em class='fa fa-envelope'></em> Contact

Pour contacter les modérateurs du site *Agenda du Libre*:

* Par courrier électronique, à l'adresse moderateurs@agendadulibre.org
* Par [IRC](http://fr.wikipedia.org/wiki/IRC), sur le canal `#agendadulibre` du réseau *Freenode*" },

    { locale: 'fr', key: 'pages.rules.content',
      value: "## <em class='fa fa-gavel'></em> Recommandations sur la modération

Tous les événements de l'Agenda du Libre passent par une phase de modération, qui permet de s'assurer que les événements recensés rentrent bien dans la [ligne éditoriale](#ligne) et que la [qualité de la description des événements](#qualite) reste bonne.

### <a name='ligne'>Ligne éditoriale</a>

Tout d'abord, les événements acceptés sont ceux qui concernent le Logiciel Libre ou le monde du Libre. Un événement simplement en rapport avec les nouvelles technologies n'a pas sa place dans l'Agenda du Libre.

Ensuite, l'Agenda du Libre a été principalement créé pour recenser les événements de la communauté du Logiciel Libre. Les événements organisés par les associations, groupes d'utilisateurs, médiathèques, bibliothèques, lieux d'accès publics à Internet concernant les Logiciels Libres sont la \"cible\" principale de l'Agenda.

Les événements organisés par les entreprises peuvent être acceptés, à condition:

* Que l'accès soit ouvert à tous. Une inscription préalable peut-être nécessaire;
* Que l'inscription soit gratuite ou à un tarif raisonnable. Certains événements de la communauté, comme le forum PHP, sont également payants, et font partie de la ligne éditoriale. Évidemment, le terme raisonnable reste sujet à interprétation. Au jour d'aujourd'hui, un événement dont l'inscription coûte 50-100 Euros peut éventuellement être accepté dans l'Agenda du Libre. Pour apprécier si un événement payant doit être validé ou non, le thème de celui-ci rentrera en compte: une conférence d'interêt général devra être validée, mais pas une formation coûteuse ou un événement de marketing pur pour les produits d'une entreprise;
* Que la formulation de la description de l'événement ne soit pas une publicité outrancière, style communiqué de presse, pour la ou les entreprises organisatrices.

En cas de doute sur le fait que l'événement soit dans la ligne éditoriale de l'agenda, ne pas hésiter à en discuter sur la liste des modérateurs, `moderateurs@agendadulibre.org`.

### <a name='qualite'>Qualité des descriptions</a>

D'autre part, une attention particulière doit être portée à l'orthographe et à la grammaire. Les événements soumis sont à ce niveau, comme beaucoup de soumissions sur d'autres sites, de qualité variable. Il convient donc de corriger au maximum les fautes avant de valider les événements. Quelques recommandations:

* Le titre de l'événement ne doit contenir ni la date, ni le lieu, et être dans la mesure du possible assez bref;
* Le lien de la ville, qui pointe vers Wikipédia, doit être fonctionnel. Il peut être nécessaire de corriger des typos dans le nom de la ville (absence d'accents ou de tirets) pour rendre le lien fonctionnel;
* La description ne doit pas être dans un style télégraphique, mais être rédigée;
* La description doit donner la date, l'heure et le lieu précis de l'événement, une description de celui-ci et le public visé. La description de l'événement doit être la plus compréhensible possible pour un néophyte;
* Les tags ne doivent pas contenir le nom de la ville ou des mots comme \"logiciel\" ou \"libre\", qui ne sont pas des tags pertinents sur l'Agenda du Libre. Par contre, ils doivent au moins contenir les noms des associations et organisations porteuses de l'événement, ainsi que les logiciels et outils qui seront abordés. On peut également préciser le type d'événement, conférence, atelier, install-party. Par exemple `toulibre gimp atelier` est une bonne liste de tags. Les tags doivent être en lettres minuscules, séparés par des espaces. Si un tag doit contenir plusieurs mots, il faudra les séparer par des tirets. Exemple: `install-party`

Par ailleurs, le travail de modération ne se limite pas à accepter ou refuser des événements et à y corriger des fautes. Il faut également:

* Vérifier la provenance de l'information: vérifier qu'un lien donnant plus d'information sur l'événement est disponible, auprès d'une source sûre (site d'un GULL, par exemple);
* S'assurer que le code HTML est potable;
* Améliorer la description de l'événement: ajouter un lien vers le site du GULL, vers un logiciel ou un projet si l'événement concerne un logiciel ou projet particulier, etc.

Si des informations manquent, les récupérer sur le site de l'événement si elles sont disponibles. Si elles ne le sont pas, envoyer un courriel au soumetteur de l'événement en demandant ces informations, et en suggérant de les ajouter également sur le site officiel si c'est pertinent. Si la description de l'événement est outrageusement incomplète et que l'événement n'a pas lieu dans les prochains jours, il est également possible de refuser en utilisant la raison « pas assez d'informations ».

### Évolution des règles de modération

Ces recommandations de modération sont à discuter et à améliorer au fur et à mesure de la vie du site. Nous pouvons en discuter via la liste `moderateurs@agendadulibre.org`.
" }
  ]
)
# rubocop:enable Metrics/LineLength
