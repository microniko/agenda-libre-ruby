# The top level controller, where can be centralised almost everything
class ApplicationController < ActionController::Base
  before_action :set_paper_trail_whodunnit, :set_filters
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery prepend: true, with: :exception

  private

  def set_locale
    I18n.locale =
      http_accept_language.compatible_language_from I18n.available_locales
  end

  # Mechanism to manage the region filter
  def set_filters
    if params.include? :region
      session[:region] = params[:region] == 'all' ? nil : params[:region].to_i
    end
    params[:region] ||= session[:region]
  end

  protected

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end
end
