# Event life cycle
# This is a central part to this project
class EventsController < ApplicationController
  has_scope :region, :locality, :tag, :daylimit, :year
  has_scope :near, type: :hash, using: %i[location distance]

  before_action :set_events, only: [:index]
  before_action :set_event, except: %i[index new preview_create create]
  before_action :set_create_event, only: %i[new preview_create create]
  before_action :check_secret, only: %i[edit preview update destroy]
  before_action :set_mailer_host
  rescue_from ActiveRecord::StaleObjectError, with: :locked

  def index
    respond_to do |format|
      format.html
      format.json { @events = @events.future }
      format.rss { @events = filter_for_rss }
      format.ics { @events = @events.last_year }
      format.xml
    end
  end

  # GET /events/new
  def new; end

  # POST /events/preview
  def preview_create
    @event.valid?
    render action: :new
  end

  # POST /events
  # POST /events.json
  def create
    respond_to do |format|
      if @event.save
        format.html { redirect_to :root, notice: t('.ok') }
        # 201 means :created
        format.json { render action: 'show', status: 201, location: @event }
      else
        format.html { render action: 'new' }
        status = :unprocessable_entity
        format.json { render json: @event.errors, status: status }
      end
    end
  end

  # PATCH/PUT /events/1/preview
  def preview
    @event.attributes = event_params
    @event.valid?
    render action: :edit
  end

  def edit; end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update event_params
        format.html { redirect_to @event, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        status = :unprocessable_entity
        format.json { render json: @event.errors, status: status }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.reason = params[:event][:reason]
    @event.destroy
    respond_to do |format|
      format.html { redirect_to :root, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  def set_events
    @events = apply_scopes Event.moderated
  end

  # Use callbacks to share common setup or constraints between actions
  def set_event
    @event = Event.moderated
    @event = Event.where secret: params[:secret] if params[:secret].present?
    @event = @event.find params[:id]
  end

  def set_create_event
    time = Time.zone.now.change(min: 0) + 1.day
    params[:event] ||= {
      start_time: time,
      end_time: time + 1.hour,
      region_id: session[:region]
    }
    @event = Event.new event_params
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through
  def event_params
    params.require(:event)
          .permit :lock_version, :title, :start_time, :end_time, :repeat, :rule,
                  :description, :place_name, :address, :city, :region_id,
                  :locality, :url, :contact, :submitter, :tag_list
  end

  def filter_for_rss
    if params[:future] == 'false'
      @events.order 'id desc'
    else
      @events.future.order('id desc').limit 20
    end
  end

  def locked
    redirect_to edit_event_url(@event, secret: @event.secret),
                alert: t('staleObjectError')
  end

  # Check that you can only edit an existing event if you know its secret
  def check_secret
    redirect_to :root, alert: t(:forbidden, scope: %i[events edit]) \
      unless params[:secret] == @event.secret
  end
end
