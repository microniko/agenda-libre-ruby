$(document).on 'turbolinks:load', ->
  $('#event_start_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_end_time').val($('#event_start_time').val())
  $('#event_end_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_start_time').val($('#event_end_time').val())

	# Quick mechanism so that the ice cube rule only appears when useful
  $('#event_repeat').each ->
    if $(this).val() == '0'
      $('.field.rule').hide()

    $(this).change ->
      if $(this).val() > 0
        $('.field.rule').show()
        $('.field.rule input').attr 'required', 'required'
      else
        $('.field.rule').hide()
        $('.field.rule input').removeAttr 'required'

  # Manage event tags edition
  $('#event_tags').each ->
    elt = $(this)
    $.ajax
      url: '/tags.json'
    .done (data) ->
      tags = jQuery.map data, (n) -> n[0]
