source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Necessary for an unknown reason in production
gem 'mini_portile2'

# The central piece of this application: the month calendar view
gem 'simple_calendar'
# The recurrence management library
gem 'ice_cube'

gem 'rails', '>= 5.1.4'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

gem 'haml-rails'

gem 'devise'
gem 'devise-i18n'
gem 'http_accept_language'
gem 'rails-i18n'
# Store some specific content, like application name and static pages contents
gem 'i18n-active_record', require: 'i18n/active_record'
# Remove leading and trailing white spaces
gem 'strip_attributes'
# Track changes to models' data
gem 'paper_trail'
gem 'piwik_analytics'

# Validate mails submitted
gem 'email_validator'
# Email address obfuscation
gem 'actionview-encoded_mail_to'
# To display a patched diff for event descriptions
gem 'differ'

# A generic library to administrate the tool
gem 'activeadmin'
gem 'has_scope'

# Markdown display
gem 'kramdown'

gem 'flag-icons-rails'
gem 'geocoder'

# Tiny MCE integration
gem 'tinymce-rails'
gem 'tinymce-rails-langs'

gem 'acts-as-taggable-on'
gem 'tagsinput-rails'

# Rails engine for static pages
gem 'high_voltage'

# SEO optimisations
gem 'meta-tags'

gem 'twitter'

group :development, :test do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Access an IRB console on exception pages or by using <%= console %>
  # anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application
  # running in the background. Read more:
  # https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Use Puma as the app server
  gem 'puma', '~> 3.7'

  # Scanning the Gemfile for insecure dependencies based on published CVEs
  gem 'bundler-audit'

  gem 'guard-bundler', require: false
  gem 'guard-livereload', require: false
  gem 'guard-migrate', require: false
  gem 'guard-minitest', require: false
  gem 'guard-rubocop', require: false
  gem 'haml_lint'
  gem 'rack-livereload'
  # Security checks
  gem 'brakeman', require: false
  gem 'guard-brakeman', require: false
  gem 'guard-rails', require: false
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  # To test objects assigned in controllers, much easier...
  gem 'rails-controller-testing'
  gem 'selenium-webdriver'
end

group :development, :test, :assets do
  # Use CoffeeScript for .coffee assets and views
  gem 'coffee-rails'
  # Use SASS for stylesheets
  gem 'sass-rails'

  # Use jquery as the JavaScript library
  gem 'jquery-ui-rails'
  # Turbolinks makes following links in your web application
  # faster. Read more: https://github.com/rails/turbolinks
  gem 'turbolinks'
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', group: :doc

  gem 'modernizr-rails'

  # A superb font to use as icons
  gem 'font-awesome-sass'

  # Carte openstreetmap
  source 'https://rails-assets.org' do
    gem 'rails-assets-jquery-sparkline'
    gem 'rails-assets-leaflet'
    gem 'rails-assets-leaflet.markercluster'
  end
end

group :assets do
  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier'
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', platforms: :ruby
end

group :production do
  # Use postgresql as the database for Active Record
  # gem 'pg'
  # Use mysql as the database for Active Record
  gem 'mysql2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
