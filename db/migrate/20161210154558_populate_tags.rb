# Migrate tags from inline attribute to separate table
class PopulateTags < ActiveRecord::Migration
  def self.up
    change_column_null :events, :tags, true
    change_column_null :orgas, :tags, true

    execute('SELECT id, tags FROM events;').each do |result|
      event = Event.find_by id: result[0]
      event.tag_list = result[1]
      event.save
    end
  end

  def self.down
    change_column_null :events, :tags, false
    change_column_null :orgas, :tags, false
  end
end
