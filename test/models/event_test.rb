require 'test_helper'

# Test events, which are the application central part
class EventTest < ActiveSupport::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    @event = events :one
  end

  test 'basic event' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: Time.zone.now + 1.hour,
      description: 'et hop!',
      city: City.first,
      region: Region.first,
      url: 'http://example.com',
      contact: 'contact@example.com',
      submitter: 'submitter@example.com',
      tag_list: 'hello world'
    )
    assert_difference 'Event.count' do
      assert @event.save, @event.errors.messages
    end

    assert_equal 32, @event.secret.size
    assert_equal 32, @event.moderator_mail_id.size
    assert_equal 32, @event.submitter_mail_id.size
  end

  test 'validations' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: Time.zone.now + 1.hour,
      description: 'et hop!',
      city: City.first,
      region: Region.first,
      url: 'http://example.com',
      contact: 'contact@example.com',
      tag_list: 'hello world'
    )

    assert @event.valid?, @event.errors.messages
    assert_equal @event.contact, @event.submitter

    @event.contact = 'hop@@@'
    assert !@event.valid?, @event.errors.messages

    @event.contact = 'contact@example.com'
    assert @event.valid?, @event.errors.messages

    # Check invalid url
    @event.url = 'htt://truc.com'
    assert !@event.valid?, @event.errors.messages

    @event.url = 'http:/truc.com'
    assert !@event.valid?, @event.errors.messages
  end

  test 'moderation' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: Time.zone.now + 1.hour,
      description: 'et hop!',
      city: City.first,
      region: Region.first,
      url: 'http://example.com',
      contact: 'contact@example.com',
      tag_list: 'hello world'
    )

    assert @event.save, @event.errors.messages
    assert !@event.moderated?

    @event.update moderated: 1

    assert @event.moderated?, @event.errors.messages
  end

  test 'named scope future.daylimit' do
    assert Event.respond_to? :future
    assert Event.respond_to? :daylimit
    assert_match(/<= end_time/, Event.future.daylimit(nil).to_sql)
    assert_match(/end_time <=/, Event.future.daylimit(nil).to_sql)
  end

  test 'named scope year' do
    assert Event.respond_to? :year
    assert_match(/<= end_time/, Event.year(2014).to_sql)
    assert_match(/start_time <=/, Event.year(2014).to_sql)
  end

  test 'named scope region' do
    assert Event.respond_to? :region
    assert_not_nil Event.region Region.first.id
  end

  test 'geo data reset' do
    # Setup geo data
    @event.latitude = 3
    @event.longitude = 3

    # Change address to ensure geo data is reset
    @event.address = 'hello world'

    assert @event.valid?, @event.errors.messages
    assert @event.save
    assert_equal 40.7143528, @event.latitude
    assert_equal(-74.0059731, @event.longitude)
  end

  test 'full address' do
    @event.address = 'hello'
    @event.city = 'world'
    @event.region.name = 'here'
    @event.region.code = 'xyz'
    assert_equal 'hello, world, here', @event.full_address
  end
end
