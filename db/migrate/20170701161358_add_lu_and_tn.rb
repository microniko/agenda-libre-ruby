# Add the new countries Luxembourg and Tunisia
class AddLuAndTn < ActiveRecord::Migration
  def self.up
    say 'Creates LU and TN regions'
    return if Region.exists? name: 'Luxembourg'
    [
      { name: 'Luxembourg', code: :LU, url: '//www.agendadulibre.org' },
      { name: 'Tunisie', code: :TN, url: '//www.agendadulibre.org' }
    ].each { |country| Region.create country }
  end
end
