# Event management life cycle
class ModerationsController < ApplicationController
  has_scope :region, :locality, :tag, :daylimit, :year
  has_scope :near, type: :hash, using: %i[location distance]

  before_action :authenticate_user!
  before_action :set_moderation, :set_mailer_host, only:
    %i[show edit preview update validate accept refuse destroy]
  before_action :generate_destroy_reason, only: :destroy
  rescue_from ActiveRecord::StaleObjectError, with: :locked

  def index
    @events = Event.unmoderated
    @orgas = Orga.unmoderated
  end

  def preview
    @moderation.attributes = moderation_params
    @moderation.valid?
    render action: :edit
  end

  # PATCH/PUT /moderations/1
  # PATCH/PUT /moderations/1.json
  def update
    respond_to do |format|
      if @moderation.update moderation_params
        format.html { redirect_to :moderations, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        status = :unprocessable_entity
        format.json { render json: @moderation.errors, status: status }
      end
    end
  end

  # PATCH/PUT /accept/1
  # PATCH/PUT /accept/1.json
  def accept
    @moderation.update moderated: true
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  # DELETE /moderations/1
  # DELETE /moderations/1.json
  def destroy
    if @moderation.reason == 'r_0'
      @moderation.paper_trail.without_versioning :destroy
    else
      @moderation.destroy!
    end
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_moderation
    @event = Event.find params[:id]
    @moderation = @event
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def moderation_params
    params.require(:event)
          .permit :lock_version, :title, :start_time, :end_time, :repeat, :rule,
                  :description, :place_name, :address, :city, :region_id,
                  :locality, :url, :contact, :submitter, :tag_list, :reason
  end

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  def locked
    redirect_to edit_moderation_url(@moderation), alert: t('staleObjectError')
  end

  def generate_destroy_reason
    @moderation.attributes = moderation_params
    return if params[:reason] == 'r_4'
    @moderation.reason = t "moderations.refuse.reason_#{params[:reason]}_long"
  end
end
