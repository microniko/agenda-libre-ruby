# Adding a diaspora URL to organisations
class AddDiasporaToOrga < ActiveRecord::Migration
  def change
    add_column :orgas, :diaspora, :text
  end
end
