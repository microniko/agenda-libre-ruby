# The foreign key from event and lug to region was badly named
class RenameRegionForeignKey < ActiveRecord::Migration
  def change
    rename_column :events, :region, :region_id
    rename_column :lugs, :region, :region_id
  end
end
