# Adding keywords to organisations
class AddTagsToOrga < ActiveRecord::Migration
  def change
    add_column :orgas, :tags, :string, null: true, default: ''
  end
end
