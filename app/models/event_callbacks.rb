# All the mail and tweet related callbacks to event's lifecycle
# also the scheduled events
class EventCallbacks
  def self.before_create(event)
    event.secret ||= SecureRandom.urlsafe_base64(32)[0...32]
    event.moderator_mail_id ||= SecureRandom.urlsafe_base64(32)[0...32]
    event.submitter_mail_id ||= SecureRandom.urlsafe_base64(32)[0...32]
  end

  def self.after_create(event)
    return unless event.event.nil?
    EventMailer.create(event).deliver_now!
    ModerationMailer.create(event).deliver_now!
  end

  def self.before_update(event)
    return unless event.will_save_change_to_moderated? && event.moderated?
    event.decision_time = Time.zone.now
    create_repeats event if event.repeat.try(:positive?) && event.rule
  end

  def self.after_update(event)
    return unless ActionMailer::Base.default_url_options[:host]

    if event.saved_change_to_moderated? && event.moderated?
      tweet event

      # Send an acceptation mail to its author
      EventMailer.accept(event).deliver_now

      # Send an acceptation mail to moderators
      ModerationMailer.accept(event).deliver_now

    else
      # Send an update mail to moderators
      ModerationMailer.update(event).deliver_now

      # Included to Send an update mail to its author
      EventMailer.update(event).deliver_now
    end
  end

  def self.after_destroy(event)
    return unless ActionMailer::Base.default_url_options[:host]
    return if event.reason == 'r_0'
    EventMailer.destroy(event).deliver_now
    ModerationMailer.destroy(event).deliver_now
  end

  # Create multiple events corresponding to a repetition
  def self.create_repeats(event)
    event.schedule.last(event.repeat).each do |schedule|
      event.events.build create_sub_event(event, schedule)
    end
  end

  def self.create_sub_event(event, schedule)
    att = event.attributes.reject do |a|
      a == 'id' || a == 'lock_version' || a == 'tags'
    end
    att[:start_time] = schedule.start_time
    att[:end_time] = schedule.end_time
    att[:tag_list] = event.tag_list
    att
  end

  # Tweet this event, if configured using apache/system variables!
  def self.tweet(event)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
      config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
      config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
      config.access_token_secret = ENV['TWITTER_ACCESS_SECRET']
    end
    client.update event.to_tweet if client.consumer_key
  end
end
